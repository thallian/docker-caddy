FROM golang:alpine as builder

RUN apk --no-cache add git

WORKDIR /go/src

RUN go get -u github.com/mholt/caddy
RUN go get -u github.com/caddyserver/builds
RUN go get -u github.com/caddyserver/dnsproviders/route53

RUN sed -i '/This is where other plugins get plugged in (imported)/a _ "github.com/caddyserver/dnsproviders/route53"' /go/src/github.com/mholt/caddy/caddy/caddymain/run.go

WORKDIR /go/src/github.com/mholt/caddy/caddy
RUN go run build.go -goos=linux -goarch=amd64

FROM registry.gitlab.com/thallian/docker-alpine-s6:master

COPY --from=builder /go/src/github.com/mholt/caddy/caddy/caddy /bin/caddy

ENV CADDY_CA "https://acme-v01.api.letsencrypt.org/directory"
ENV CADDYPATH "/etc/ssl/caddy"

RUN addgroup -g 2222 caddy
RUN adduser -h /home/caddy -S -D -u 2222 -G caddy caddy

RUN apk add --no-cache libressl libcap ca-certificates
RUN setcap cap_net_bind_service=+ep /bin/caddy

RUN mkdir /etc/caddy/ && mkdir /etc/ssl/caddy && mkdir /www

ADD /rootfs /

EXPOSE 80 443
VOLUME /etc/ssl/caddy /www
