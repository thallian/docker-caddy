[Caddy](https://caddyserver.com/) server with
[route53 dns plugin](https://github.com/caddyserver/dnsproviders).

# Ports
- 80
- 443

# Volumes
- /etc/ssl/caddy
- /www

# Environment Variables
## CADDY_CA
- default: "https://acme-v01.api.letsencrypt.org/directory"

URL to certificate authority's ACME server directory.

## CADDY_ACME_EMAIL
Default ACME CA account email address.

# Capabilities
- CHOWN
- DAC_OVERRIDE
- NET_BIND_SERVICE
- SETGID
- SETUID
